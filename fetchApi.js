import {setLoading} from "./modified_json_parcel/main.js";
let isLoading = true;
export async function fetchApi(){
    const res= await fetch("https://words.dev-apis.com/word-of-the-day");;
    const resObj= await res.json();
    const word=await resObj.word.toUpperCase();
    isLoading = false;
    setLoading(isLoading);
    return word;
};

export {isLoading};