
const letters=document.querySelectorAll('.box-letter');
const animation_spiral = document.querySelector(".infospiral");
let row=0;
let currentGuess='';
let currentGparts;
const answerLength=5;
const round=6;
let done =false;


export function commit(word){
    const wordParts=word.split("");

    if(currentGuess.length!== answerLength){
        return;
    }

    currentGparts=currentGuess.split("");
    const map =  makeMap(wordParts);


    for(let i=0;i<5;i++){
        if(wordParts[i]===currentGparts[i]){
            letters[row*answerLength+i].classList.add("correct");
            map[currentGparts[i]]--;
        }else if(wordParts.includes(currentGparts[i]) && map[currentGparts[i]]>0){
            letters[row*answerLength+i].classList.add("close");
            map[currentGparts[i]]--;
        }else{
            letters[row*answerLength+i].classList.add("wrong");
        }
    }

    row++;
    
    if(currentGuess===word){
        document.querySelector(".infobar").innerText="Congratulations You won the Game, play again by refreshing the page";
        done = true;
        return;
    }
    else if (row== 6) {
        document.querySelector(".infobar").innerText="You loose the game, try again by refreshing the page.";
        done = true;
        return;
    }
    currentGuess='';

}


export function backspace(){
    currentGuess=currentGuess.substring(0 , currentGuess.length-1);
    letters[answerLength*row+currentGuess.length].innerText="";
}

export function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter);
}

export function addLetter(letter){
    if(currentGuess.length<answerLength){
        currentGuess+=letter;
    }
    else {
        currentGuess=currentGuess.substring(0 , currentGuess.length-1)+letter;
    }
    letters[answerLength*row+currentGuess.length-1].innerText=letter;
}


export function setLoading(isLoading) {
    animation_spiral.classList.toggle("hidden", !isLoading);
}

export function makeMap(array) {
    const obj = {};
    for (let i = 0; i < array.length; i++) {
      if (obj[array[i]]) {
        obj[array[i]]++;
      } else {
        obj[array[i]] = 1;
      }
    }
    return obj;
  }
export {done} ;