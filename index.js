import {fetchApi,isLoading} from "../fetchApi.js";
import {commit,backspace , isLetter, addLetter,done} from "./main.js"


export async function init(){
    const word= await fetchApi();
    document.addEventListener('keydown', function handleKeyPress(event){
        if (done || isLoading) {
            return;
          }
        const action=event.key;
        if(action==='Enter'){

            commit(word);
        }
        else if(action==='Backspace'){
            backspace();
        }
        else if(isLetter(action)){
            addLetter(action.toUpperCase());
        }
        else{

        }
    });
}

init();